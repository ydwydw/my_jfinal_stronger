package com.hyl.anno.cache;


import com.hyl.model._enum.cacheused.ElementChosen;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface CGet {
    ElementChosen value() default ElementChosen.CACHEANDMATHOD;
}
