package com.hyl.anno.login;

import java.lang.annotation.*;

/**
 * Created by huangyili on 2017/6/20.
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Login {
}
