package com.hyl.config.Jfinal;

import com.hyl.config.cdn_resource.CdnResource;
import com.hyl.config.mysql.MysqlProperties;
import com.hyl.config.redis.RedisProperties;
import com.hyl.config.statusAndMessage.SAM;
import com.hyl.controller.UserController;
import com.hyl.controller.index.IndexController;
import com.hyl.inteceptors.catche.CatcheInterceptor;
import com.hyl.inteceptors.login.LoginInterceptor;
import com.hyl.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.sql.SqlKit;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

import java.io.File;
import java.util.Enumeration;

/**
 * Created by huangyili on 2017/6/20.
 */
public class JfinalConfig extends JFinalConfig {
    public static boolean SHOW_SQL;

    public JfinalConfig() {
        super();
        this.loadPropertyFile(new File(SqlKit.class.getClassLoader().getResource("config.properties").getFile()), "UTF-8");
        MysqlProperties.JDBC_DRIVER=getProperty("jdbc.driver", "");
        MysqlProperties.JDBC_PASSWORD=getProperty("jdbc.password","");
        MysqlProperties.JDBC_URL=getProperty("jdbc.url","root");
        MysqlProperties.JDBC_USERNAME=getProperty("jdbc.username","root");


        JfinalConfig.SHOW_SQL=getPropertyToBoolean("config.showsql");

        System.out.println("redisConfig start-------------------");
        RedisProperties.cacheName=getProperty("config.redis.cacheName","redis");
        RedisProperties.host=getProperty("config.redis.host");
        RedisProperties.port=getPropertyToInt("config.redis.port",6379);
        RedisProperties.maxIdle=getPropertyToInt("config.redis.maxIdle");
        RedisProperties.maxTotal=getPropertyToInt("config.redis.maxTotal");
        RedisProperties.onBorrow=getPropertyToBoolean("config.redis.onBorrow");
        RedisProperties.onReturn=getPropertyToBoolean("config.redis.onReturn");




        Prop props = PropKit.use(new File(SqlKit.class.getClassLoader().getResource("statusAndMessage.properties").getFile()));
        try {
            Enumeration en = props.getProperties().keys();
            while (en.hasMoreElements()) {
                String name = en.nextElement().toString();
                String path = props.getProperties().getProperty(name);
                SAM.sam.put(name, path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        CdnResource.CDNRESOURCE=getProperty("cdn.resource","root");
    }

    public void configConstant(Constants constants) {
        constants.setJsonFactory(new FastJsonFactory());
        constants.setDevMode(true);

    }

    public void configRoute(Routes routes) {

        routes.add("/", IndexController.class);
        routes.add("/user", UserController.class);
    }

    public void configEngine(Engine engine) {

    }

    public void configPlugin(Plugins plugins) {

        DruidPlugin druidPlugin = new MysqlProperties().getdruidPlugin();
        plugins.add(druidPlugin);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setShowSql(JfinalConfig.SHOW_SQL);
        arp.setDialect(new MysqlDialect());//添加Mysql的方言
        arp.setContainerFactory(new CaseInsensitiveContainerFactory());// 配置属性名(字段名)大小写不敏感容器工厂
        plugins.add(arp);
//        自动化搞定Model与表的映射
        _MappingKit.mapping(arp);
        plugins.add(new RedisProperties().getRedisPlugin());

    }

    /**
     * 拦截器的配置
     * @param interceptors
     */
    public void configInterceptor(Interceptors interceptors) {
        //添加全局的登录拦截器
        interceptors.addGlobalActionInterceptor(new LoginInterceptor());
        interceptors.addGlobalServiceInterceptor(new CatcheInterceptor());
    }

    public void configHandler(Handlers handlers) {

    }

//    public void afterJFinalStart() {
//        try {
//            FreeMarkerRender.getConfiguration().setSharedVariable("resource", GuiYangWebConfig.CDN_RESOURCES);
//        } catch (TemplateModelException e) {
//            e.printStackTrace();
//        }
//    }
}
