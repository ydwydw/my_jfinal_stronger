package com.hyl.config.mysql;

import com.jfinal.plugin.druid.DruidPlugin;

/**
 * Created by huangyili on 2017/6/20.
 */
public class MysqlProperties {
    /**
     * 数据库连接URL,数据库用户名，数据库密码，驱动
     */
    public static String JDBC_URL;
    public static String JDBC_USERNAME;
    public static String JDBC_PASSWORD;
    public static String JDBC_DRIVER;
    public DruidPlugin getdruidPlugin(){
        DruidPlugin druidPlugin = new DruidPlugin(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD, JDBC_DRIVER);
        druidPlugin.setMinIdle(60);
        druidPlugin.setMaxPoolPreparedStatementPerConnectionSize(1000);
        return druidPlugin;
    }
}
