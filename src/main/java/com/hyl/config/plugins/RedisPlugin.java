package com.hyl.config.plugins;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.IPlugin;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.IKeyNamingPolicy;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.serializer.FstSerializer;
import com.jfinal.plugin.redis.serializer.ISerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisPlugin implements IPlugin {
    private String cacheName;
    private String host;
    private Integer port=6379;
    private Integer timeout=2*60*60;
    private String password;
    private Integer database;
    private String clientName;
    private ISerializer serializer;
    private IKeyNamingPolicy keyNamingPolicy;
    private JedisPoolConfig jedisPoolConfig;
    private JedisPool jedisPool;
    private  boolean onBorrow=false;
    private  boolean onReturn=false;
    private  Integer maxIdle=200;
    private  Integer maxTotal=300;

    public RedisPlugin(String cacheName, String host, Integer port, Integer timeout, String password, boolean onBorrow, boolean onReturn, Integer maxIdle, Integer maxTotal) {
        this.cacheName = cacheName;
        this.host = host;
        this.port = port;
        this.timeout = timeout;
        this.password = password;
        this.onBorrow = onBorrow;
        this.onReturn = onReturn;
        this.maxIdle = maxIdle;
        this.maxTotal = maxTotal;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    public boolean isOnBorrow() {
        return onBorrow;
    }

    public void setOnBorrow(boolean onBorrow) {
        this.onBorrow = onBorrow;
    }

    public boolean isOnReturn() {
        return onReturn;
    }

    public void setOnReturn(boolean onReturn) {
        this.onReturn = onReturn;
    }

    public RedisPlugin(String cacheName, String host) {
        this.port = null;
        this.timeout = null;
        this.password = null;
        this.database = null;
        this.clientName = null;
        this.serializer = null;
        this.keyNamingPolicy = null;
        this.jedisPoolConfig = new JedisPoolConfig();
        if(StrKit.isBlank(cacheName)) {
            throw new IllegalArgumentException("cacheName can not be blank.");
        } else if(StrKit.isBlank(host)) {
            throw new IllegalArgumentException("host can not be blank.");
        } else {
            this.cacheName = cacheName.trim();
            this.host = host;
        }
    }

    public RedisPlugin(String cacheName, String host, int port) {
        this(cacheName, host);
        this.port = Integer.valueOf(port);
    }

    public RedisPlugin(String cacheName, String host, int port, int timeout) {
        this(cacheName, host, port);
        this.timeout = Integer.valueOf(timeout);
    }

    public RedisPlugin(String cacheName, String host, int port, int timeout, String password) {
        this(cacheName, host, port, timeout);
        if(StrKit.isBlank(password)) {
            throw new IllegalArgumentException("password can not be blank.");
        } else {
            this.password = password;
        }
    }

    public RedisPlugin(String cacheName, String host, int port, int timeout, String password, int database) {
        this(cacheName, host, port, timeout, password);
        this.database = Integer.valueOf(database);
    }

    public RedisPlugin(String cacheName, String host, int port, int timeout, String password, int database, String clientName) {
        this(cacheName, host, port, timeout, password, database);
        if(StrKit.isBlank(clientName)) {
            throw new IllegalArgumentException("clientName can not be blank.");
        } else {
            this.clientName = clientName;
        }
    }

    public RedisPlugin(String cacheName, String host, int port, String password) {
        this(cacheName, host, port, 2000, password);
    }

    public RedisPlugin(String cacheName, String host, String password) {
        this(cacheName, host, 6379, 2000, password);
    }

    public boolean start() {
//        JedisPool jedisPool;
//        if(this.port != null && this.timeout != null && this.password != null && this.database != null && this.clientName != null) {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host, this.port.intValue(), this.timeout.intValue(), this.password, this.database.intValue(), this.clientName);
//        } else if(this.port != null && this.timeout != null && this.password != null && this.database != null) {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host, this.port.intValue(), this.timeout.intValue(), this.password, this.database.intValue());
//        } else if(this.port != null && this.timeout != null && this.password != null) {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host, this.port.intValue(), this.timeout.intValue(), this.password);
//        } else if(this.port != null && this.timeout != null) {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host, this.port.intValue(), this.timeout.intValue());
//        } else if(this.port != null) {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host, this.port.intValue());
//        } else {
//            jedisPool = new JedisPool(this.jedisPoolConfig, this.host);
//        }
        JedisPoolConfig config = new JedisPoolConfig();
        //最大空闲连接数, 应用自己评估，不要超过ApsaraDB for Redis每个实例最大的连接数
        config.setMaxIdle(maxIdle);
        //最大连接数, 应用自己评估，不要超过ApsaraDB for Redis每个实例最大的连接数
        config.setMaxTotal(maxTotal);
        config.setTestOnBorrow(onBorrow);
        config.setTestOnReturn(onReturn);
//        String host = "*.aliyuncs.com";
//        String password = "密码";
        jedisPool = new JedisPool(config, host, port, timeout, password);
        if(this.serializer == null) {
            this.serializer = FstSerializer.me;
        }

        if(this.keyNamingPolicy == null) {
            this.keyNamingPolicy = IKeyNamingPolicy.defaultKeyNamingPolicy;
        }

        Cache cache = new Cache(this.cacheName, jedisPool, this.serializer, this.keyNamingPolicy);
        Redis.addCache(cache);
        return true;
    }

    public boolean stop() {
        Cache cache = Redis.removeCache(this.cacheName);
//        if(cache == Redis.mainCache) {
//            Redis.mainCache = null;
//        }
//
//        cache.jedisPool.destroy();
        jedisPool.destroy();
        return true;
    }

    public JedisPoolConfig getJedisPoolConfig() {
        return this.jedisPoolConfig;
    }

    public void setSerializer(ISerializer serializer) {
        this.serializer = serializer;
    }

    public void setKeyNamingPolicy(IKeyNamingPolicy keyNamingPolicy) {
        this.keyNamingPolicy = keyNamingPolicy;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.jedisPoolConfig.setTestWhileIdle(testWhileIdle);
    }

    public void setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
        this.jedisPoolConfig.setMinEvictableIdleTimeMillis((long)minEvictableIdleTimeMillis);
    }

    public void setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
        this.jedisPoolConfig.setTimeBetweenEvictionRunsMillis((long)timeBetweenEvictionRunsMillis);
    }

    public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
        this.jedisPoolConfig.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
    }


}
