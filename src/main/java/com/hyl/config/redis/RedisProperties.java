package com.hyl.config.redis;


import com.hyl.config.plugins.RedisPlugin;

/**
 * Created by huangyili on 2017/6/20.
 */
public class RedisProperties {
    public static String host="localhost";
    public static Integer port=6379;
    public static String password;
    public static boolean onBorrow=false;
    public static boolean onReturn=false;
    public static Integer maxIdle=200;
    public static Integer maxTotal=300;
    public static Integer timeout=2*60*60;
    public static String cacheName="redis";
    public RedisPlugin getRedisPlugin(){
        RedisPlugin redisPlugin = new RedisPlugin( cacheName,  host,  port,  timeout,  password,  onBorrow,  onReturn,  maxIdle,  maxTotal);
        return redisPlugin;
    }
}
