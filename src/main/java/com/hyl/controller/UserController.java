package com.hyl.controller;

import com.hyl.anno.login.Login;
import com.hyl.controller.base.BaseController;
import com.hyl.model.User;
import com.hyl.service.user.UserService;
import com.hyl.service.user.impl.UserServiceImpl;

/**
 * Created by huangyili on 2017/6/20.
 */
@Login
public class UserController extends BaseController {

    UserService userService=getService();

    public UserService getService(){
        UserService enhance = enhance(UserServiceImpl.class);
        return enhance;
    }
    public void test(){
        User byId = User.dao.findById(1l);
        System.out.println(byId);
        renderJson(byId);
    }

    public void index(){
        System.out.println("controller层开始工作");

        User user = this.userService.findUserById(getParaToLong("id"));
        renderJson(user);
    }

}
