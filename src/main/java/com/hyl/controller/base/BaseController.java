package com.hyl.controller.base;

import com.hyl.config.statusAndMessage.SAM;
import com.jfinal.core.Controller;

/**
 * Created by huangyili on 2017/6/20.
 */
public class BaseController extends Controller {
    public String getResult(int status){
        return SAM.sam.get(status+"")==null?"没有"+status+"对应的状态码":SAM.sam.get(status+"");
    }
}
