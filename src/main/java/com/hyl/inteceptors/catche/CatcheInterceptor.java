package com.hyl.inteceptors.catche;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hyl.anno.cache.CGet;
import com.hyl.anno.cache.CPut;
import com.hyl.model._enum.cacheused.ElementChosen;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.json.Json;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.render.Render;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * 缓存的拦截器
 * @author HYL
 * @create 2017-06-20 下午9:43
 **/
public class CatcheInterceptor implements Interceptor {

    public void intercept(Invocation invocation) {
        System.out.println("xxxxx111111");
        Cache redis = Redis.use();
        Method method = invocation.getMethod();
        String methodName = method.getName();
        String className = invocation.getTarget().getClass().getName();
        Parameter[] parameters = method.getParameters();
        StringBuilder sb=new StringBuilder();
        String key = sb.append(className)
                .append("/")
                .append(methodName)
                .append("/")
                .append(parameters.toString()).toString();
        if (method.isAnnotationPresent(CPut.class)) {//如果是要把内容保存到redis中
            try {
//            获得方法的返回值
                invocation.invoke();
                Object obj  = invocation.getReturnValue();
//          把返回值存入到缓存中，key为这个方法的所有的方法参数等相关信息
                String string = JSONObject.toJSONString(obj);
                redis.set(key,string);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        if (method.isAnnotationPresent(CGet.class)) {
            //获取注解里面的值
            String obj = ((String) redis.get("222"));
            Class<?> clazz = method.getReturnType();
            Object o = JSONObject.parseObject(obj, clazz);
            ElementChosen elementChosens = method.getAnnotation(CGet.class).value();
            if (elementChosens.equals(ElementChosen.CACHEONLY)) {
                System.out.println("无论如何都从缓存中获取值并返回");

                invocation.setReturnValue(o);
            } else {
                if (obj != null) {
                    System.out.println("只有换从中有值才从缓存中获取并返回");
                    invocation.setReturnValue(o);
                } else {
                    System.out.println("第一次进来没有缓存，把返回值丢进缓存");
                    try {
//            获得方法的返回值
                        invocation.invoke();
                        Object returnValue = invocation.getReturnValue();
                        String string = JSONObject.toJSONString(returnValue);
                        redis.set(222+"",string);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            }

        }else {
            invocation.invoke();
        }
    }
}
