package com.hyl.inteceptors.login;

import com.alibaba.fastjson.JSONObject;
import com.hyl.anno.login.Login;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import java.lang.reflect.Method;

/**
 * Created by huangyili on 2017/6/20.
 */
public class LoginInterceptor implements Interceptor {

    public void intercept(Invocation invocation) {
        Controller controller = invocation.getController();
        Method method = invocation.getMethod();
        Class<?> clazz = method.getDeclaringClass();

        if (clazz.isAnnotationPresent(Login.class)||method.isAnnotationPresent(Login.class)) {

            if (!isLogin()) {//需要验证登入，从缓存中获取用户看看是否为空
                //用户未登入
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("status",2);
                jsonObject.put("message","请登录");
                controller.renderJson(jsonObject);
            }else {
                invocation.invoke();
            }
        }else {
            invocation.invoke();
        }
    }

    /**
     * 判断是否登录的方法
     * @return
     */
    public boolean isLogin(){

        return true;
    }

}
