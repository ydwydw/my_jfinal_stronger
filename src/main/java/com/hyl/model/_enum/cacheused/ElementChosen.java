package com.hyl.model._enum.cacheused;

public enum ElementChosen {
    /**
     * 只从缓存中获取
     */
    CACHEONLY,
    /**
     * 先从缓存中获取，如果没有再从方法中获取
     */
    CACHEANDMATHOD;
}
