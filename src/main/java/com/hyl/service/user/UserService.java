package com.hyl.service.user;

import com.hyl.model.User;

/**
 * Created by huangyili on 2017/6/20.
 */

public interface UserService {
    public User findUserById(Long id);
}
