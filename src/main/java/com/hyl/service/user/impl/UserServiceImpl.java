package com.hyl.service.user.impl;

import com.hyl.anno.cache.CGet;
import com.hyl.model.User;
import com.hyl.service.user.UserService;

/**
 * Created by huangyili on 2017/6/20.
 */
public class UserServiceImpl implements UserService {
    @CGet
    public User findUserById(Long id) {
        System.out.println("service层开始工作");
        return User.dao.findById(id);
    }
}
